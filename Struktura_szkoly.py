class Czlowiek:

    def __init__(self, imie,nazwisko, wiek, plec):
        self.wiek = wiek
        self.plec = plec
        self.imie = imie
        self.nazwisko = nazwisko

class Student(Czlowiek):

    def __init__(self, imie, nazwisko, wiek, plec, szkola, studia, rok ):
        super().__init__(imie, nazwisko, wiek, plec)
        self.szkola = szkola
        self.studia = studia
        self.rok = rok 

class Nauczyciel(Czlowiek):

    def __init__(self, imie, nazwisko, wiek, plec, szkola, pozycja, przedmiot ):
        super().__init__(imie, nazwisko, wiek, plec)
        self.szkola = szkola
        self.pozycja = pozycja
        self.przedmiot = przedmiot

class Pracownik(Czlowiek):
    def __init__(self, imie, nazwisko, wiek, plec, pozycja, wynagrodzenie ):
        super().__init__(imie, nazwisko, wiek, plec)
        self.pozycja = pozycja
        self.wynagrodzenie = wynagrodzenie


class Szkola:

    def __init__(self, nazwa, adres ):
        self.nazwa = nazwa
        self.adres = adres


class Klasy(Szkola):

    def __init__(self, nazwa, adres, rodzaje_klasy, max_studentow):
        super().__init__(nazwa, adres)
        self.rodzaje_klas = rodzaje_klasy
        self.max_studentow = max_studentow
        self.studenci = []

    def add_student(self, student):
        if len(self.studenci) < self.max_studentow:
            self.studenci.append(student)
        else: 
            print("Klasa jest pelna")


class Pietra(Szkola):
    
    def __init__(self, nazwa, adres, nr_pietra):
        super().__init__(nazwa, adres)
        self.nr_pietra = nr_pietra

class Lekcja(Szkola):
    
    def __init__(self, nazwa, adres, rodzaj, godzina, nauczyciel, student):
        super().__init__(nazwa, adres)
        self.rodzaj = rodzaj
        self.godzina = godzina
        self.nauczyciel = nauczyciel
        self.student = student






